import org.scalatest._
import SkylineProblem._


class SkylineProblemSpec extends FlatSpecLike with Assertions
 {
  "Two skylines" should "merge into one skyline" in {
    val skyline1 = List((1, 11), (4, 13), (6, 0)).map(Point.fromTuple)
    val skyline12 = List((2, 6), (5, 15), (7, 0)).map(Point.fromTuple)
    val result = merge(skyline1, skyline12)
    val expectedResult = List((1, 11), (4, 13), (5, 15), (7, 0)).map(Point.fromTuple)
    assert(result === expectedResult)
  }

  "mergeAll" should "work with provided example input and output" in {
    val buildings = List((1, 11, 5), (2, 6, 7), (3, 13, 9), (12, 7, 16), (14, 3, 25), (19, 18, 22), (23, 13, 29), (24, 4, 28))
    val initialSkylines = buildings2Skyline(buildings)
    val expected = List(1, 11, 3, 13, 9, 0, 12, 7, 16, 3, 19, 18, 22, 3, 23, 13, 29, 0)
    assert (mergeAll(initialSkylines) === expected)
  }

  "Skylines with points with the same X" should "merge into skyline with the point to be the highest of the two" in {
    val skyline1 = List((1, 11), (2, 9), (10, 0)).map(Point.fromTuple)
    val skyline2 = List((1, 11), (2, 10), (10, 0)).map(Point.fromTuple)

    val result = merge(skyline1, skyline2)
    val expectedResult = List((1, 11), (2, 10), (10, 0)).map(Point.fromTuple)
    assert(result === expectedResult)
  }

  it should "merge into skyline with the point to be the highest of the two when supplied in reverse order" in {
    val skyline1 = List((1, 11), (2, 9), (10, 0)).map(Point.fromTuple)
    val skyline2 = List((1, 11), (2, 10), (10, 0)).map(Point.fromTuple)

    val result = merge(skyline2, skyline1)
    val expectedResult = List((1, 11), (2, 10), (10, 0)).map(Point.fromTuple)
    assert(result === expectedResult)
  }

  "Skylines with points with the same X (2)" should "merge into skyline with the point to be the highest of the two" in {
    val skyline1 = List((1, 11), (2, 12), (10, 0)).map(Point.fromTuple)
    val skyline2 = List((1, 11), (2, 13), (10, 0)).map(Point.fromTuple)

    val result = merge(skyline1, skyline2)
    val expectedResult = List((1, 11), (2, 13), (10, 0)).map(Point.fromTuple)
    assert(result === expectedResult)
  }

  it should "merge into skyline with the point to be the highest of the two when supplied in reverse order" in {
    val skyline1 = List((1, 11), (2, 12), (10, 0)).map(Point.fromTuple)
    val skyline2 = List((1, 11), (2, 13), (10, 0)).map(Point.fromTuple)

    val result = merge(skyline2, skyline1)
    val expectedResult = List((1, 11), (2, 13), (10, 0)).map(Point.fromTuple)
    assert(result === expectedResult)
  }

  "Skylines with points with the same X (3)" should "merge into skyline with the point to be the highest of the two" in {
    val skyline1 = List((1, 11), (2, 12), (10, 0)).map(Point.fromTuple)
    val skyline2 = List((1, 11), (2, 9), (10, 0)).map(Point.fromTuple)

    val result = merge(skyline2, skyline1)
    val expectedResult = List((1, 11), (2, 12), (10, 0)).map(Point.fromTuple)
    assert(result === expectedResult)
  }

  it should "merge into skyline with the point to be the highest of the two when supplied in reverse order" in {
    val skyline1 = List((1, 11), (2, 12), (10, 0)).map(Point.fromTuple)
    val skyline2 = List((1, 11), (2, 9), (10, 0)).map(Point.fromTuple)

    val result = merge(skyline1, skyline2)
    val expectedResult = List((1, 11), (2, 12), (10, 0)).map(Point.fromTuple)
    assert(result === expectedResult)
  }

  "Skylines with several points on the same X" should "merge into skyline with the point to be the highest of the two last points with the same X" in {
    val skyline1 = List((1, 11), (2, 13), (2, 12), (10, 0)).map(Point.fromTuple)
    val skyline2 = List((1, 11), (2, 9), (10, 0)).map(Point.fromTuple)

    val result = merge(skyline1, skyline2)
    val expectedResult = List((1, 11), (2, 12), (10, 0)).map(Point.fromTuple)
    assert(result === expectedResult)
  }




}
