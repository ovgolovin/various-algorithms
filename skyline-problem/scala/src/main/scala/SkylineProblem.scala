

object SkylineProblem {

  object Skyline extends Enumeration {
    val A, B = Value
  }

  case object Point {
    def fromTuple(tuple: (Int, Int)) = new Point(tuple._1, tuple._2)
  }

  case class Point(x: Int, height: Int)

  /**
   * Class to accumulate points from 2 skylines.
   * Points must be fed in non-descending order
   */
  class Accumulator() {
    private val builder = List.newBuilder[Point]
    private var lastPoint, lastASkylinePoint, lastBSkylinePoint = Option.empty[Point]

    /**
     * Add next point. Points must be added in non-descending order
     * @param fromSkyline Skyline this point is from
     * @param point Point to add
     */
    def add(fromSkyline: Skyline.Value, point: Point) {
      updateSkyline(fromSkyline: Skyline.Value, point: Point)
      lastPoint match {
        case None => // first point
          lastPoint = Some(point)
        case Some(Point(lastX, lastHeight)) => {
          val topmostPoint = Point(point.x, getPointHeight(fromSkyline, point)) // point or point from other skyline horizon (if it's higher)
          val Point(newX, newHeight) = topmostPoint
          if (lastHeight == newHeight) {
            // ignore points with the same height
          } else if (lastX == newX) {  // in case of the same X coordinate
            lastPoint = Some(topmostPoint)  // replace lastPoint with the new topmostPoint
            // It won't be lower then the replaced point, as topmostPoint
            // height is no lower the the oppisite skyline horizon
            // which equal the height of replaced point
          } else {  // just add new point
            lastPoint.foreach({ point => builder += point})  // move previous last point to buffer
            lastPoint = Some(topmostPoint)
          }
        }
      }
    }

    /**
     * Update skyline current height with the new point
     */
    private def updateSkyline(fromSkyline: Skyline.Value, point: Point) {
      fromSkyline match {
        case Skyline.A => lastASkylinePoint = Some(point)
        case Skyline.B => lastBSkylinePoint = Some(point)
      }
    }

    /**
     * Get the maximum of this point height or the opposite horizon.
     * @param fromSkyline
     * @param point
     * @return
     */
    private def getPointHeight(fromSkyline: Skyline.Value, point: Point) = {
      val oppositeSkyline = fromSkyline match {
        case Skyline.A => lastBSkylinePoint
        case Skyline.B => lastASkylinePoint
      }
      oppositeSkyline match {
        case None => point.height
        case Some(Point(_, oppHeight)) =>
          oppHeight max point.height
      }
    }

    /**
     * Get resultant list
     * This will invalidate current accumulator for later usage.
     */
    def result() = {
      lastPoint.foreach({ point => builder += point})
      builder.result()
    }
  }

  /**
   * Unite two skylines
   */
  def merge(A: List[Point], B: List[Point]) = {
    /**
     * Recursive merger
     * @param acc Accumulator collecting accending points from both skylines
     * @param aPart Left points from first skyline
     * @param bPart Left points from second skyline
     * @return Resultant skyline from accumulator in the first argument
     */
    def helper(acc: Accumulator, aPart: List[Point], bPart: List[Point]): List[Point] = {
      if (aPart.isEmpty && bPart.isEmpty) {
        acc.result()
      } else if (aPart.isEmpty) {
        acc.add(Skyline.B, bPart.head)
        helper(acc, aPart, bPart.tail)
      } else if (bPart.isEmpty) {
        acc.add(Skyline.A, aPart.head)
        helper(acc, aPart.tail, bPart)
      } else {
        if (aPart.head.x < bPart.head.x) {
          acc.add(Skyline.A, aPart.head)
          helper(acc, aPart.tail, bPart)
        } else {
          acc.add(Skyline.B, bPart.head)
          helper(acc, aPart, bPart.tail)
        }
      }
    }
    helper(new Accumulator(), A, B)
  }

  /**
   * Iterative skyline merger.
   * Merge skylines by pairs, until there is only one skyline.
   * @param singles
   * @return
   */
  def mergeAll(singles: List[List[Point]]) = {
    var result = singles
    while (result.size > 1) {
      result = result.grouped(2).toList.map {
        case (List(a, b)) => merge(a, b)
      }
    }
    result.flatten.map { case Point(x, height) => List(x, height)}.flatten
  }

  def buildings2Skyline(buildings: List[(Int, Int, Int)]): List[List[Point]] = {
    buildings.view.map({
      case building@(a, height, b) =>
        if (a >= b || height <= 0) throw new IllegalArgumentException(s"Invalid building: $building")
        List(Point(a, height), Point(b, 0))
    }
    ).toList
  }

  def main(args: Array[String]) {



  }
}