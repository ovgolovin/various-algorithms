import heapq
import itertools
import collections
import unittest



def skyline_decorator(points, skyline_number):
    """
    Add index of skyline to points
    """
    return ((x, height, skyline_number) for (x, height) in points)   


def last_value_of_iterator(it):
    return collections.deque(it, 1).pop()


def merge(skyline1, skyline2):
    last_skyline_points = [None, None]
    skyline1_dec = skyline_decorator(skyline1, 0) 
    skyline2_dec = skyline_decorator(skyline2, 1) 
    all_dec_points = heapq.merge(skyline1_dec, skyline2_dec)
    def non_filtered_points(points):
        for (x, height, skyline_num) in points:
            opp_skyline_point = last_skyline_points[1 - skyline_num]
            last_skyline_points[skyline_num] = (x, height)
            if opp_skyline_point is None:
                yield (x, height)
            else:
                yield (x, max((height, opp_skyline_point[1])))
    filtered_by_x = (last_value_of_iterator(it) for (key, it) in itertools.groupby(non_filtered_points(all_dec_points), lambda point: point[0]))
    filtered_by_h = (next(it) for (key, it) in itertools.groupby(filtered_by_x, lambda point: point[1]))
    return list(filtered_by_h)
            
           

def merge_all(skylines):
    def merge_skylines_by_pairs(skylines):
        for (skyline1, skyline2) in itertools.izip(*[iter(skylines)]*2):
            yield merge(skyline1, skyline2)
    while(len(skylines) > 1):
        skylines = list(merge_skylines_by_pairs(skylines))
    return [value for skyline in skylines for point in skyline for value in point]
            


def skylines_from_buildings(buildings):
    return [[(a, height), (b, 0)] for (a, height, b) in buildings]


class Tests(unittest.TestCase):
    def test_normal_merge(self):
        skyline1 = [(1, 11), (4, 13), (6, 0)]
        skyline2 = [(2, 6), (5, 15), (7, 0)]
        result = merge(skyline1, skyline2)
        expected_result = [(1, 11), (4, 13), (5, 15), (7, 0)]
        self.assertEqual(expected_result, result)


    def test_provided_example(self):
        buildings = [(1, 11, 5), (2, 6, 7), (3, 13, 9), (12, 7, 16), (14, 3, 25), (19, 18, 22), (23, 13, 29), (24, 4, 28)]
        skylines = skylines_from_buildings(buildings)
        result = merge_all(skylines)
        expected_result = [1, 11, 3, 13, 9, 0, 12, 7, 16, 3, 19, 18, 22, 3, 23, 13, 29, 0]
        self.assertEqual(expected_result, result)


    def test_same_x_coordinates_2(self):
        skyline1 = [(1, 11), (2, 9), (10, 0)]
        skyline2 = [(1, 11), (2, 10), (10, 0)]
        result = merge(skyline1, skyline2)
        expected_result = [(1, 11), (2, 10), (10, 0)]
        self.assertEqual(expected_result, result)


    def test_same_x_coordinates_3(self):
        skyline1 = [(1, 11), (2, 9), (10, 0)]
        skyline2 = [(1, 11), (2, 10), (10, 0)]
        result = merge(skyline1, skyline2)
        expected_result = [(1, 11), (2, 10), (10, 0)]
        self.assertEqual(expected_result, result)


    def test_same_x_coordinates_3(self):
        skyline1 = [(1, 11), (2, 12), (10, 0)]
        skyline2 = [(1, 11), (2, 13), (10, 0)]
        result = merge(skyline1, skyline2)
        expected_result = [(1, 11), (2, 13), (10, 0)]
        self.assertEqual(expected_result, result)
        

    def test_same_x_coordinates_3(self):
        skyline1 = [(1, 11), (2, 12), (10, 0)]
        skyline2 = [(1, 11), (2, 13), (10, 0)]
        result = merge(skyline1, skyline2)
        expected_result = [(1, 11), (2, 13), (10, 0)]
        self.assertEqual(expected_result, result)


    def test_same_x_coordinates_3(self):
        skyline1 = [(1, 11), (2, 12), (10, 0)]
        skyline2 = [(1, 11), (2, 13), (10, 0)]
        result = merge(skyline2, skyline1)
        expected_result = [(1, 11), (2, 13), (10, 0)]
        self.assertEqual(expected_result, result)


    def test_same_x_coordinates_3(self):
        skyline1 = [(1, 11), (2, 12), (10, 0)]
        skyline2 = [(1, 11), (2, 9), (10, 0)]
        result = merge(skyline1, skyline2)
        expected_result = [(1, 11), (2, 12), (10, 0)]
        self.assertEqual(expected_result, result)


    def test_same_x_coordinates_3(self):
        skyline1 = [(1, 11), (2, 12), (10, 0)]
        skyline2 = [(1, 11), (2, 9), (10, 0)]
        result = merge(skyline2, skyline1)
        expected_result = [(1, 11), (2, 12), (10, 0)]
        self.assertEqual(expected_result, result)
        

    def test_several_points_with_the_same_x_on_one_skyline(self):
        '''
        This shouldn't happen normally
        '''
        skyline1 = [(1, 11), (2, 13), (2, 12), (10, 0)]
        skyline2 = [(1, 11), (2, 9), (10, 0)]
        result = merge(skyline1, skyline2)
        expected_result = [(1, 11), (2, 12), (10, 0)]
        self.assertEqual(expected_result, result)


    def test_same_height(self):
        skyline1 = [(1, 11), (10, 0)]
        skyline2 = [(5, 11), (9,0)]
        result = merge(skyline1, skyline2)
        expected_result = [(1, 11), (10, 0)]
        self.assertEqual(expected_result, result)



if __name__ == '__main__':
    unittest.main()
