#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdexcept>

/**
 * Convert int into * char.
 * Caller is responsible for clearing memory with the result.
 */
char * int_to_string(int x) {
	int length;
	char * result;
	if (x < 0) {
		throw std::runtime_error("x is less than 0");
	}
	if (x == 0) {
		length = 1;
		result = (char *) malloc(2);
		*result = '0';
	} else {
		length = floor(log10(x)) + 1;
		int offset = '0';
		result = (char *) malloc(length + 1);  // for 0-terminator
		for(int i = length - 1; i >= 0; --i) {
			*(result + i) = (x % 10) + offset;
			x /= 10;
		}
	}
	*(result + length) = 0;
	return result;
}


int main(int argv, const char * const * args) {
	const int ints[] = {0, 1, 9, 10, 12, 99, 102};
	for (int i = 0; i < sizeof(ints) / sizeof(ints[0]); ++i) {
		char * s = int_to_string(ints[i]);
		printf("%d -> %s\n", ints[i], s);
		free(s);
	}
}
